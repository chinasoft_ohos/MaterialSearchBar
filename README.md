# MaterialSearchBar

#### 项目介绍
- 项目名称：MaterialSearchBar
- 所属系列：openharmony的第三方组件适配移植
- 功能：实现搜索和侧滑
- 基线版本：Release 0.8.5
- 项目移植状态：主功能完成
- 调用差异：无
- 开发版本：sdk6，DevEco Studio 2.2 Beta1

#### 效果演示
<img src="https://gitee.com/chinasoft_ohos/MaterialSearchBar/raw/master/img/demo.gif"></img>

#### 安装教程
 
1、在项目根目录下的build.gradle文件中
 ```gradle
allprojects {
    repositories {
        maven {
            url 'https://s01.oss.sonatype.org/content/repositories/releases/'
        }
    }
}
```
2.在entry模块的build.gradle文件中
 ```gradle
 dependencies {
    implementation('com.gitee.chinasoft_ohos:MaterialSearchBar:1.0.0')
    ......  
 }
```

在sdk6，DevEco Studio 2.2 Beta1下项目可直接运行
如无法运行，删除项目.gradle,.idea,build,gradle,build.gradle文件，
并依据自己的版本创建新项目，将新项目的对应文件复制到根目录下

#### 使用说明

使用该库非常简单，只需查看提供的示例的源代码。（查看ViewCreateHelper.java中在ScrollView和ListContainer中的使用）
```示例xml
Provided Styles are: MaterialSearchBarLight and MaterialSearchBarDark
Example:
<com.balysv.materialripple.MaterialRippleLayout
                    ohos:height="match_parent"
                    ohos:width="match_content"
                    ohos:background_element="#ffffff"
                    ohos:layout_alignment="horizontal_center"
                    ohos:weight="1"
                    app:mrl_rippleAlpha="0.2"
                    app:mrl_rippleColor="#80000000"
                    app:mrl_rippleOverlay="true">

                    <Image
                        ohos:id="$+id:main_item"
                        ohos:height="match_parent"
                        ohos:width="match_content"
                        ohos:image_src="$media:ic_dots_vertical_black_48dp"
                        ohos:text_size="50"/>

                </com.balysv.materialripple.MaterialRippleLayout>
```

```
private void onFocusChangedListener() {
        main_tf.setFocusChangedListener((component, isFocused) -> {
            if (isFocused) {
                depen_layout.setContentPositionY(1080);
            } else { 
                depen_layout.setContentPositionY(1900);
            }
        });
    }
private void initStatusBarColor() {
        try {
            int color = getResourceManager().getElement(ResourceTable.Color_barColor).getColor();
            WindowManager.getInstance().getTopWindow().get().setStatusBarColor(color);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
AnimatorValue value = new AnimatorValue();
    value.setDuration(1000);
    value.setLoopedCount(0);
    value.setCurveType(Animator.CurveType.LINEAR);
    value.setValueUpdateListener((animatorValue, v) -> {
        if (v >= 0.3f) {
            list.setVisibility(Component.HIDE);
        }
        list.setContentPosition(0, (1 - v) * (main_tf.getHeight() + 30));
    });
    value.start();

```
另外，您可以选择在布局xml文件中设置以下属性，以自定义ExpandableTextView的行为。
1. mt_speechMode 如果设置为true，将显示麦克风图标，而不是搜索图标

2. mt_maxSuggestionsCount 指定存储的最大搜索查询数

3. mt_speechIconDrawable 在语音模式为true时设置语音图标的drawable

4. mt_roundedSearchBarEnabled 在v21 +上使用胶囊状的搜索栏，并在较低的位置还原为默认设置

5. mt_hint 设置搜索栏为焦点且搜索查询为空时的提示文本

#### 测试信息

CodeCheck代码测试无异常

CloudTest代码测试无异常

病毒安全检测通过

当前版本demo功能与原项目组件基本无差异


#### 版本迭代

- 1.0.0

#### 版权和许可信息

 MIT