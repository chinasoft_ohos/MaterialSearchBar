package com.mancj.materialsearchbar.adapter;

import com.mancj.materialsearchbar.ResourceTable;
import com.mancj.materialsearchbar.bean.HistoryBean;
import ohos.aafwk.ability.AbilitySlice;
import ohos.agp.components.*;

import java.util.List;

public class HistoryItemProvider extends BaseItemProvider {
    private List<HistoryBean> list;
    private AbilitySlice slice;
    private int layout;

    public HistoryItemProvider(List<HistoryBean> list, AbilitySlice slice, int layout) {
        this.list = list;
        this.slice = slice;
        this.layout = layout;
    }

    @Override
    public int getCount() {
        return list == null ? 0 : list.size();
    }

    @Override
    public Object getItem(int i) {
        if (list != null && i >= 0 && i < list.size()) {
            return list.get(i);
        }
        return null;
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public Component getComponent(int i, Component component, ComponentContainer componentContainer) {
        final Component comp;
        if (component == null) {
            comp = LayoutScatter.getInstance(slice).parse(layout, null, false);
        } else {
            comp = component;
        }
        HistoryBean historyBean = list.get(i);
       Text text = (Text) comp.findComponentById(ResourceTable.Id_text);
       text.setText(historyBean.getName());
       return comp;
    }
}
