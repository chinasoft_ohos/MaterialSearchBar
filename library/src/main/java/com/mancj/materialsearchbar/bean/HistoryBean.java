package com.mancj.materialsearchbar.bean;

public class HistoryBean {
    private String name;

    /**
     * HistoryBean
     *
     * @param name 历史记录
     */
    public HistoryBean(String name) {
        this.name = name;
    }
    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }
}
