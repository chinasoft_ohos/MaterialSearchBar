package com.mancj.example;

import com.mancj.materialsearchbar.adapter.HistoryItemProvider;
import com.mancj.materialsearchbar.bean.HistoryBean;
import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.content.Intent;
import ohos.agp.animation.Animator;
import ohos.agp.animation.AnimatorProperty;
import ohos.agp.animation.AnimatorValue;
import ohos.agp.colors.RgbColor;
import ohos.agp.components.Component;
import ohos.agp.components.DependentLayout;
import ohos.agp.components.DirectionalLayout;
import ohos.agp.components.element.ShapeElement;
import ohos.agp.components.Image;
import ohos.agp.components.ListContainer;
import ohos.agp.components.Text;
import ohos.agp.components.TextField;
import ohos.agp.utils.Color;
import ohos.agp.utils.LayoutAlignment;
import ohos.agp.window.dialog.ToastDialog;
import ohos.agp.window.service.WindowManager;
import ohos.eventhandler.EventHandler;
import ohos.hiviewdfx.HiLog;
import ohos.hiviewdfx.HiLogLabel;
import ohos.media.image.ImageSource;
import ohos.media.image.PixelMap;
import ohos.media.image.common.PixelFormat;
import ohos.media.image.common.Rect;
import ohos.media.image.common.Size;
import ohos.multimodalinput.event.MmiPoint;
import ohos.multimodalinput.event.TouchEvent;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.concurrent.atomic.AtomicReference;

import static com.mancj.example.ResourceTable.Color_purple_300;

public class NavAbilitySlice extends AbilitySlice implements Animator.StateChangedListener, Component.ClickedListener {
    private static final boolean ISPD = true;
    private static final boolean ISPDS = false;
    private static final HiLogLabel LABLE = new HiLogLabel(HiLog.LOG_APP, 0x002, "SearchBarDemo");
    private static final int IS_SIXTY = 60;
    private static final int IS_ONETHOUSANDANDEIGHTY = 1080;
    private static final int IS_ONETHOUSANDANDEIGHTYS = 1400;
    private static final int IS_ONETHOUSANDANDNINEHUNDRED = 1900;
    private static final int ISTWOHUNDREDANDFIFTYFIVE = 255;
    private static final int ISONEHUNDREDANDNINETYTWO = 192;
    private static final int ISTWOHUNDREDANDSIXTY = 260;
    private static final int ISONETHOUSAND = 1000;
    private static final int ISTWOTHOUSAND = 2000;
    private static final int ISTHIRTY = 30;
    private static final int ISTHREE = 3;
    private static final int ISFIVEHUNDRED = 500;
    private static final int ISONEHUNDRED = 100;
    private static final int ISTHREEHUNDREDANDFIFTY = 350;
    private static final int ISTWOHUNDRED = 200;
    private static final int ISTWOHUNDREDANDSEVENTY = 270;
    private static final int ISONEHUNDREDANDEIGHTY = 180;
    private static final int ISTWO = 2;
    private static final int ISTHREEHUNDREDANDSIXTY = 360;
    private static final int ISSEVENHUNDRED = -700;
    private static final int ISFOUR = 4;
    private static final int ISEIGHT = 8;
    private static final float ISZEROPOINTTHREE = 0.3f;
    private static final float ISZEROPOINTNINE = 0.9f;
    private static final float ISONETHOUSANDF = 1000.0f;
    private DirectionalLayout lyImport;
    private DirectionalLayout lyGallery;
    private DirectionalLayout lySlideshow;
    private DirectionalLayout lyTools;
    private DirectionalLayout lyShare;
    private DirectionalLayout header;
    private DirectionalLayout lySend;
    private DirectionalLayout mDrawerLayout;
    private DirectionalLayout mMainLayout;
    private Image ivImport;
    private Image ivGallery;
    private Image ivSlideshow;
    private Image ivTools;
    private Image mainSearch;
    private Image mDrawerImage;
    private RoundRectView roundRectView;
    private Text tvImport;
    private Text tvGallery;
    private Text tvSlideshow;
    private Text tvTools;
    private ShapeElement element;
    private ShapeElement elements;
    private ShapeElement elementTwo;
    private Image mainItem;
    private TextField mainTf;
    private DependentLayout depenLayout;
    private AnimatorProperty mAnimatorIcon;
    private AnimatorProperty mAnimatorLayout;
    private AnimatorProperty animatorProperty;
    private boolean isSelOpen = ISPD;
    private boolean isFlag = ISPDS;
    private boolean isItem = ISPD;
    private ListContainer list;
    private ListContainer dialog;
    private int count = 0;
    private HistoryItemProvider provider;
    private HistoryItemProvider itemProvider;
    private ArrayList<HistoryBean> beans = new ArrayList<>();
    private ArrayList<HistoryBean> beans1 = new ArrayList<>();
    private Image search;
    private AtomicReference<Float> temp = new AtomicReference<>((float) 0);

    private EventHandler handler;
    private HandlerRunClick mHandlerRunClick = new HandlerRunClick();

    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_ability_nav);
        initStatusBarColor();
        initView();
        initData();
        OnClickListener();
    }

    private void initStatusBarColor() {
        try {
            int color = getResourceManager().getElement(ResourceTable.Color_barColor).getColor();
            /*
             * 状态栏颜色
             */
            WindowManager.getInstance().getTopWindow().get().setStatusBarColor(color);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void initData() {
        setBackGround(search);
        /*
         * 设置图片，ID需要先编译后才能识别到，当前图片资源放置在resources/base/media/test.png
         */
        roundRectView.putPixelMap(getPixelMap(ResourceTable.Media_robot));
        roundRectView.setScaleMode(Image.ScaleMode.ZOOM_CENTER);
        DirectionalLayout.LayoutConfig layoutConfig = new DirectionalLayout.LayoutConfig(
                DirectionalLayout.LayoutConfig.MATCH_PARENT, DirectionalLayout.LayoutConfig.MATCH_PARENT);
        header.addComponent(roundRectView, layoutConfig);
        list.setItemProvider(provider);
        for (int ia = 1; ia <= ISTHREE; ia++) {
            beans1.add(new HistoryBean("Item " + ia));
        }
        itemProvider = new HistoryItemProvider(beans1, NavAbilitySlice.this, ResourceTable.Layout_item_layout);
        dialog.setItemProvider(itemProvider);

        element = new ShapeElement();
        elements = new ShapeElement();
        elementTwo = new ShapeElement();
        try {
            element.setRgbColor(
                    RgbColor.fromArgbInt(getResourceManager().getElement(ResourceTable.Color_bg).getColor()));
        } catch (Exception e) {
            e.printStackTrace();
        }
        elements.setRgbColor(
                new RgbColor(ISTWOHUNDREDANDFIFTYFIVE, ISTWOHUNDREDANDFIFTYFIVE, ISTWOHUNDREDANDFIFTYFIVE));
        elementTwo.setRgbColor(
                new RgbColor(ISONEHUNDREDANDNINETYTWO, ISONEHUNDREDANDNINETYTWO, ISONEHUNDREDANDNINETYTWO));
    }

    private void initView() {
        search = (Image) findComponentById(ResourceTable.Id_search);
        lyImport = (DirectionalLayout) findComponentById(ResourceTable.Id_ly_import);
        ivImport = (Image) findComponentById(ResourceTable.Id_iv_import);
        tvImport = (Text) findComponentById(ResourceTable.Id_tv_import);
        lyGallery = (DirectionalLayout) findComponentById(ResourceTable.Id_ly_gallery);
        ivGallery = (Image) findComponentById(ResourceTable.Id_iv_gallery);
        tvGallery = (Text) findComponentById(ResourceTable.Id_tv_gallery);
        lySlideshow = (DirectionalLayout) findComponentById(ResourceTable.Id_ly_slideshow);
        ivSlideshow = (Image) findComponentById(ResourceTable.Id_iv_slideshow);
        tvSlideshow = (Text) findComponentById(ResourceTable.Id_tv_slideshow);
        lyTools = (DirectionalLayout) findComponentById(ResourceTable.Id_ly_tools);
        ivTools = (Image) findComponentById(ResourceTable.Id_iv_tools);
        tvTools = (Text) findComponentById(ResourceTable.Id_tv_tools);
        lyShare = (DirectionalLayout) findComponentById(ResourceTable.Id_ly_share);
        header = (DirectionalLayout) findComponentById(ResourceTable.Id_header);
        roundRectView = new RoundRectView(this, new Size(ISTWOHUNDREDANDSIXTY, ISTWOHUNDREDANDSIXTY));

        lySend = (DirectionalLayout) findComponentById(ResourceTable.Id_ly_send);
        mDrawerImage = (Image) findComponentById(ResourceTable.Id_main_drawer);
        mDrawerImage.setClickedListener(NavAbilitySlice.this);
        mMainLayout = (DirectionalLayout) findComponentById(ResourceTable.Id_main_layout);
        mMainLayout.setClickedListener(NavAbilitySlice.this);
        mDrawerLayout = (DirectionalLayout) findComponentById(ResourceTable.Id_main_drawer_layout);
        mainItem = (Image) findComponentById(ResourceTable.Id_main_item);
        mainSearch = (Image) findComponentById(ResourceTable.Id_main_search);
        mainTf = (TextField) findComponentById(ResourceTable.Id_main_tf);
        depenLayout = (DependentLayout) findComponentById(ResourceTable.Id_depen_layout);
        list = (ListContainer) findComponentById(ResourceTable.Id_history);
        provider = new HistoryItemProvider(beans, NavAbilitySlice.this, ResourceTable.Layout_item_last_request);

        dialog = (ListContainer) findComponentById(ResourceTable.Id_dialog);
        depenLayout.setMarginTop(IS_ONETHOUSANDANDEIGHTYS);
    }

    private PixelMap getPixelMap(int drawableId) {
        InputStream drawableInputStream = null;
        try {
            drawableInputStream = getResourceManager().getResource(drawableId);
            ImageSource.SourceOptions sourceOptions = new ImageSource.SourceOptions();
            sourceOptions.formatHint = "image/png";
            ImageSource.DecodingOptions decodingOptions = new ImageSource.DecodingOptions();
            decodingOptions.desiredSize = new Size(0, 0);
            decodingOptions.desiredRegion = new Rect(0, 0, 0, 0);
            decodingOptions.desiredPixelFormat = PixelFormat.ARGB_8888;
            ImageSource imageSource = ImageSource.create(drawableInputStream, sourceOptions);
            PixelMap pixelMap = imageSource.createPixelmap(decodingOptions);
            return pixelMap;
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (drawableInputStream != null) {
                    drawableInputStream.close();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return null;
    }

    private void setBackGround(Image image) {
        try {
            ShapeElement shapeElement = new ShapeElement();
            shapeElement.setCornerRadius(image.getHeight() * ISTWO);
            shapeElement.setRgbColor(
                    RgbColor.fromArgbInt(getResourceManager().getElement(ResourceTable.Color_teal_701).getColor()));
            image.setBackground(shapeElement);
        } catch (Exception ignored) {
        }
    }

    private void hideList() {
        AnimatorValue value = new AnimatorValue();
        value.setDuration(ISFIVEHUNDRED);
        value.setLoopedCount(0);
        value.setCurveType(Animator.CurveType.LINEAR);
        value.setValueUpdateListener((animatorValue, v) -> {
            if (v >= ISZEROPOINTTHREE) {
                list.setVisibility(Component.HIDE);
            }
            list.setContentPosition(0, (1 - v) * (mainTf.getHeight() + ISTHIRTY));
        });
        value.start();
    }

    private void showList() {
        if (beans.size() != 0) {
            if (beans.size() == 1) {
                list.setHeight(150);
                depenLayout.setHeight(1550);
            } else if (beans.size() == 2) {
                list.setHeight(300);
                depenLayout.setHeight(1700);
            } else {
                list.setHeight(450);
                depenLayout.setHeight(1850);
            }
            AnimatorValue value = new AnimatorValue();
            value.setDuration(ISFIVEHUNDRED);
            value.setLoopedCount(0);
            value.setCurveType(Animator.CurveType.LINEAR);
            value.setValueUpdateListener((animatorValue, v) -> {
                if (v >= ISZEROPOINTNINE) {
                    list.setVisibility(Component.VISIBLE);
                }
                list.setContentPosition(0, v * (mainTf.getHeight() + ISTHIRTY));
            });
            value.start();
        }
    }

    @Override
    public void onActive() {
        super.onActive();
    }

    @Override
    public void onForeground(Intent intent) {
        super.onForeground(intent);
    }

    private void OnClickListener() {
        mainTf.setFocusChangedListener((component, isFocused) -> {
            if (!isFocused) {
                depenLayout.setMarginTop(IS_ONETHOUSANDANDEIGHTYS);
            } else {
                depenLayout.setMarginTop(ISFIVEHUNDRED);
            }
        });

        mainItem.setClickedListener(component -> {
            if (isItem) {
                dialog.setVisibility(Component.VISIBLE);
            } else {
                dialog.setVisibility(Component.INVISIBLE);
            }
            list.setVisibility(Component.HIDE);
            isItem = !isItem;
        });

        dialog.setItemClickedListener((listContainer, component, i, l) -> {
            getUITaskDispatcher().delayDispatch(() -> {
                isItem = true;
                dialog.setVisibility(Component.INVISIBLE);
            }, ISFIVEHUNDRED);
        });

        list.setItemClickedListener((listContainer, component, i, l) -> {
            Text text = (Text) listContainer.getComponentAt(i).findComponentById(ResourceTable.Id_text);
            text.setClickedListener(component1 -> {
                String name = beans.get(i).getName();
                mainTf.setText(name);
            });

            Image delete = (Image) listContainer.getComponentAt(i).findComponentById(ResourceTable.Id_iv_delete);
            delete.setClickedListener(component1 -> {
                if (beans.size() - 1 != 0 && i != 0) {
                    if (beans.size() == 2 && i == 2) {
                        beans.remove(1);
                    } else {
                        beans.remove(i);
                    }
                    if (beans.size() == 1) {
                        list.setHeight(150);
                    } else if (beans.size() == 2) {
                        list.setHeight(300);
                    } else {
                        list.setHeight(450);
                    }
                    provider.notifyDataChanged();
                }
            });
        });

        mainTf.setClickedListener(component -> {
            getFocusable();
            hideItem();
            if (!isFlag) {
                changeIc();
                showList();
                isFlag = true;
            }
            depenLayout.setMarginTop(ISFIVEHUNDRED);
        });

        mainSearch.setClickedListener(component -> {
            getFocusable();
            hideItem();
            if (!isFlag) {
                changeIc();
                showList();
                isFlag = true;
                depenLayout.setMarginTop(ISFIVEHUNDRED);
                mainTf.simulateClick();
            } else {
                setHint();
                mainTf.simulateClick();
                depenLayout.setMarginBottom(ISFIVEHUNDRED);
            }
            depenLayout.setMarginTop(ISFIVEHUNDRED);
        });

        search.setClickedListener(component -> {
            getFocusable();
            hideItem();
            if (!isFlag) {
                changeIc();
                showList();
                isFlag = true;
            }
            mainTf.simulateClick();
        });

        mMainLayout.setTouchEventListener((component, touchEvent) -> {
            if (touchEvent.getAction() == TouchEvent.POINT_MOVE) {
                MmiPoint pointerPosition = touchEvent.getPointerPosition(0);
                float x = pointerPosition.getX();
                if (temp.get() < x && !isFlag) {
                    showDraw();
                }
                if (temp.get() > x && isFlag) {
                    onAnimatorIcon();
                }
                if (x > ISONEHUNDRED) {
                    temp.set(x);
                } else {
                    temp.set(ISONETHOUSANDF);
                }
            }
            if (touchEvent.getAction() == TouchEvent.PRIMARY_POINT_UP) {
                temp.set(ISONETHOUSANDF);
            }
            return true;
        });
        lyImport.setClickedListener(component -> {
            lyGallery.setBackground(elements);
            lySlideshow.setBackground(elements);
            lyTools.setBackground(elements);
            ivImport.setPixelMap(ResourceTable.Media_a2);
            ivGallery.setPixelMap(ResourceTable.Media_c1);
            ivSlideshow.setPixelMap(ResourceTable.Media_d1);
            ivTools.setPixelMap(ResourceTable.Media_b1);
            try {
                int color1 = getResourceManager().getElement(Color_purple_300).getColor();
                Color color = new Color(color1);
                tvImport.setTextColor(color);
            } catch (Exception e) {
                e.printStackTrace();
            }
            tvGallery.setTextColor(Color.BLACK);
            tvSlideshow.setTextColor(Color.BLACK);
            tvTools.setTextColor(Color.BLACK);
            getUITaskDispatcher().delayDispatch(() -> {
                lyImport.setBackground(element);
                onAnimatorIcon();
            }, ISTHREEHUNDREDANDFIFTY);
        });
        lyGallery.setClickedListener(component -> {
            lyImport.setBackground(elements);
            lySlideshow.setBackground(elements);
            lyTools.setBackground(elements);
            ivImport.setPixelMap(ResourceTable.Media_a1);
            ivGallery.setPixelMap(ResourceTable.Media_c2);
            ivSlideshow.setPixelMap(ResourceTable.Media_d1);
            ivTools.setPixelMap(ResourceTable.Media_b1);
            try {
                int color1 = getResourceManager().getElement(Color_purple_300).getColor();
                Color color = new Color(color1);
                tvGallery.setTextColor(color);
            } catch (Exception e) {
                e.printStackTrace();
            }
            tvImport.setTextColor(Color.BLACK);
            tvSlideshow.setTextColor(Color.BLACK);
            tvTools.setTextColor(Color.BLACK);
            getUITaskDispatcher().delayDispatch(() -> {
                lyGallery.setBackground(element);
                onAnimatorIcon();
            }, ISTHREEHUNDREDANDFIFTY);
        });
        lySlideshow.setClickedListener(component -> {
            lyImport.setBackground(elements);
            lyGallery.setBackground(elements);
            lyTools.setBackground(elements);
            ivImport.setPixelMap(ResourceTable.Media_a1);
            ivGallery.setPixelMap(ResourceTable.Media_c1);
            ivSlideshow.setPixelMap(ResourceTable.Media_d2);
            ivTools.setPixelMap(ResourceTable.Media_b1);
            try {
                int color1 = getResourceManager().getElement(Color_purple_300).getColor();
                Color color = new Color(color1);
                tvSlideshow.setTextColor(color);
            } catch (Exception e) {
                e.printStackTrace();
            }
            tvImport.setTextColor(Color.BLACK);
            tvGallery.setTextColor(Color.BLACK);
            tvTools.setTextColor(Color.BLACK);
            getUITaskDispatcher().delayDispatch(() -> {
                lySlideshow.setBackground(element);
                onAnimatorIcon();
            }, ISTHREEHUNDREDANDFIFTY);
        });
        lyTools.setClickedListener(component -> {
            lyImport.setBackground(elements);
            lyGallery.setBackground(elements);
            lySlideshow.setBackground(elements);
            ivImport.setPixelMap(ResourceTable.Media_a1);
            ivGallery.setPixelMap(ResourceTable.Media_c1);
            ivSlideshow.setPixelMap(ResourceTable.Media_d1);
            ivTools.setPixelMap(ResourceTable.Media_b2);
            try {
                int color1 = getResourceManager().getElement(Color_purple_300).getColor();
                Color color = new Color(color1);
                tvTools.setTextColor(color);
            } catch (Exception e) {
                e.printStackTrace();
            }
            tvImport.setTextColor(Color.BLACK);
            tvGallery.setTextColor(Color.BLACK);
            tvSlideshow.setTextColor(Color.BLACK);
            getUITaskDispatcher().delayDispatch(() -> {
                lyTools.setBackground(element);
                onAnimatorIcon();
            }, ISTHREEHUNDREDANDFIFTY);
        });
        lyShare.setClickedListener(component -> {
            getUITaskDispatcher().delayDispatch(() -> {
                lyShare.setBackground(elementTwo);
                onAnimatorIcon();
            }, ISTHREEHUNDREDANDFIFTY);
        });
        lySend.setClickedListener(component -> {
            getUITaskDispatcher().delayDispatch(() -> {
                lySend.setBackground(elementTwo);
                onAnimatorIcon();
            }, ISTHREEHUNDREDANDFIFTY);
        });

        mainTf.setEditorActionListener(j -> {
            if (j == 1) {
                String text = mainTf.getText();
                if (text.length() == 0) {
                    new ToastDialog(getContext())
                            .setText("搜索内容不能为空")
                            .show();
                    depenLayout.setMarginTop(IS_ONETHOUSANDANDEIGHTYS);
                } else {
                    hideItem();
                    HistoryBean historyBean = null;
                    int ISI = 0;
                    if (!mainTf.getText().trim().equals("")) {
                        String trim = mainTf.getText().trim();
                        if (beans.size() == 0) {
                            historyBean = new HistoryBean(trim);
                        }
                        for (HistoryBean bean : beans) {
                            if (bean.getName().equals(trim)) {
                                ISI++;
                            }
                        }
                        if (0 == ISI) {
                            historyBean = new HistoryBean(trim);
                        }
                        if (historyBean != null) {
                            Collections.reverse(beans);
                            beans.add(historyBean);
                            if (beans.size() == ISFOUR) {
                                beans.remove(0);
                            }
                            Collections.reverse(beans);
                            provider.notifyDataChanged();
                        }
                        hideList();
                    }
                    depenLayout.setMarginTop(IS_ONETHOUSANDANDEIGHTYS);
                    return true;
                }
            }
            return false;
        });

        mDrawerLayout.setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
            }
        });
    }

    private void getFocusable() {
        mainTf.setFocusable(Component.FOCUS_ENABLE);
        mainTf.setEnabled(true);
        mainTf.setTouchFocusable(true);
        mainTf.requestFocus();
    }

    private void hideItem() {
        isItem = true;
        dialog.setVisibility(Component.INVISIBLE);
    }

    private void changeIc() {
        animatorProperty = mDrawerImage.createAnimatorProperty();
        animatorProperty.rotate(ISTWOHUNDREDANDSEVENTY).setDuration(ISTWOHUNDRED).start();
        mainSearch.setPixelMap(ResourceTable.Media_ic_close_black_48dp);
        setHint();
        mDrawerImage.setPixelMap(ResourceTable.Media_close);
    }

    private void changeClose() {
        animatorProperty = mDrawerImage.createAnimatorProperty();
        animatorProperty.rotate(ISONEHUNDREDANDEIGHTY).setDuration(ISTWOHUNDRED).start();
        mainSearch.setPixelMap(ResourceTable.Media_ic_magnify_black_48dp);
        setGoogle();
        mDrawerImage.setPixelMap(ResourceTable.Media_open);
    }

    private void setHint() {
        mainTf.setText("");
        if (count == 0) {
            count++;
            mainTf.setText("Hello World!");
        } else {
            mainTf.setHint("Search");
        }
        mainTf.setHintColor(Color.GRAY);
    }

    @Override
    protected void onBackground() {
        super.onBackground();
        if (mAnimatorIcon != null) {
            mAnimatorIcon = null;
        }
        if (mAnimatorLayout != null) {
            mAnimatorLayout = null;
        }
    }

    @Override
    public void onStart(Animator animator) {
    }

    @Override
    public void onStop(Animator animator) {
    }

    @Override
    public void onCancel(Animator animator) {
    }

    public void onEnd(Animator animator) {
        if (mAnimatorIcon != null && animator == mAnimatorIcon) {
            /*
             * 重置动画
             */
            mAnimatorIcon.reset();
            mAnimatorIcon.resume();
            mAnimatorIcon = null;
            /*
             * 初始化抽屉效果动画
             */
            if (mAnimatorLayout == null) {
                mAnimatorLayout = mDrawerLayout.createAnimatorProperty();
            }
            /*
             * 切换图标
             */
            if (isSelOpen) {
                /*
                 * 展示抽屉导航动画
                 */
                mAnimatorLayout.moveFromX(ISSEVENHUNDRED).moveToX(0).setDuration(ISFIVEHUNDRED);
                clearFocusable();
            } else {
                mDrawerImage.setPixelMap(ResourceTable.Media_open);
                /*
                 * 收起抽屉导航动画
                 */
                mAnimatorLayout.moveFromX(0).moveToX(ISSEVENHUNDRED).setDuration(ISFIVEHUNDRED);
                setFocusable();
            }
            /*
             * 开启动画
             */
            mAnimatorLayout.start();
            isSelOpen = !isSelOpen;
        }
    }

    private void clearFocusable() {
        mainTf.setTouchFocusable(false);
        search.setTouchFocusable(false);
        dialog.setTouchFocusable(false);
        mainSearch.setTouchFocusable(false);
        mainItem.setTouchFocusable(false);

        mainTf.setEnabled(false);
        search.setEnabled(false);
        dialog.setEnabled(false);
        mainSearch.setEnabled(false);
        mainItem.setEnabled(false);
    }

    private void setFocusable() {
        mainTf.setTouchFocusable(true);
        search.setTouchFocusable(true);
        dialog.setTouchFocusable(true);
        mainSearch.setTouchFocusable(true);
        mainItem.setTouchFocusable(true);

        mainTf.setEnabled(true);
        search.setEnabled(true);
        dialog.setEnabled(true);
        mainSearch.setEnabled(true);
        mainItem.setEnabled(true);
    }

    @Override
    public void onPause(Animator animator) {
    }

    @Override
    public void onResume(Animator animator) {
    }

    @Override
    public void onClick(Component component) {
        switch (component.getId()) {
            case ResourceTable.Id_main_drawer:
                showDraw();
                setGoogle();
                hideList();
                hideItem();
                break;
            case ResourceTable.Id_main_layout:
                /*
                 * 执行抽屉导航缩起动画
                 */
                if (!isSelOpen) {
                    if (mAnimatorIcon == null) {
                        mAnimatorIcon = mDrawerImage.createAnimatorProperty();
                    }
                    mAnimatorIcon.rotate(ISTHREEHUNDREDANDSIXTY).setDuration(ISTWOHUNDRED);
                    mAnimatorIcon.setStateChangedListener(NavAbilitySlice.this);
                    /*
                     * 开启动画
                     */
                    mAnimatorIcon.start();
                    isFlag = false;
                }
                hideItem();
                break;
            default:
        }
    }

    private void showDraw() {
        lyShare.setBackground(elements);
        lySend.setBackground(elements);
        if (!isFlag) {
            if (mAnimatorIcon == null) {
                mAnimatorIcon = mDrawerImage.createAnimatorProperty();
            }
            mAnimatorIcon.setStateChangedListener(NavAbilitySlice.this);
            /*
             * 开启动画
             */
            mAnimatorIcon.start();
            setGoogle();
        } else {
            changeClose();
        }
        mainTf.clearFocus();
        isFlag = !isFlag;
    }

    private void onAnimatorIcon() {
        if (!isSelOpen) {
            if (mAnimatorIcon == null) {
                mAnimatorIcon = mDrawerImage.createAnimatorProperty();
            }
            mAnimatorIcon.rotate(ISTHREEHUNDREDANDSIXTY).setDuration(ISTWOHUNDRED);
            mAnimatorIcon.setStateChangedListener(NavAbilitySlice.this);
            /*
             * 开启动画
             */
            mAnimatorIcon.start();
            isFlag = false;
        }
    }

    private void setGoogle() {
        mainTf.setText("");
        mainTf.setHint("Google Play");
        mainTf.setHintColor(Color.BLACK);
    }

    /**
     * 点击事件延时显示，用以解决不知道什么原因的toast和数值动画冲突
     */
    private class HandlerRunClick implements Runnable {
        @Override
        public void run() {
            new ToastDialog(NavAbilitySlice.this).setContentText("Short click")
                    .setSize(DirectionalLayout.LayoutConfig.MATCH_CONTENT, DirectionalLayout.LayoutConfig.MATCH_CONTENT)
                    .setAlignment(LayoutAlignment.BOTTOM | LayoutAlignment.HORIZONTAL_CENTER)
                    .setOffset(0, IS_SIXTY).show();
        }
    }
}
